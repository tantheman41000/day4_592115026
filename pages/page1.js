import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Text, TouchableOpacity } from 'react-native'


class Page1 extends Component {
    render() {
        const { todos, completes, addTodo } = this.props
        console.log(todos)
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => { addTodo(1); }}>
                    <Text> Add Number to todo</Text>
                </TouchableOpacity>
                <Text>
                    latest todo is : {todos[todos.length - 1]}
                </Text>
            </View>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        todos: state.todos,
        completes: state.completes
    }
}
const mapDispathToProps = (dispatch) => {
    return {
        addTodo: (topic) => {
            dispatch({
                type: 'ADD_TODO',
                topic: topic
            })
            console.log('heyyyyyy')
        }
    }
}
export default connect(mapStateToProps, mapDispathToProps)(Page1)
