import {createStore,combineReducers} from 'redux'
import TodosReducer from './TodosReducer'
import CompletesReducer from './pages/CompleteReducer';

const reducers = combineReducers({
    todos: TodosReducer,
    completes: CompletesReducer
});

const store = createStore(reducers);
export default store;